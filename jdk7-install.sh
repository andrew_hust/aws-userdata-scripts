#!/usr/bin/env bash
set -e -x

add-apt-repository -y ppa:webupd8team/java
apt-get update

echo oracle-java7-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections
apt-get -y install oracle-java7-installer oracle-java7-set-default
update-java-alternatives -s java-7-oracle

