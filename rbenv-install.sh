#!/usr/bin/env bash
set -e -x

whoami
printenv HOME
cd
git clone git://github.com/sstephenson/rbenv.git ~/.rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
git clone git://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc

export PATH="$HOME/.rbenv/bin:$HOME/.rbenv/plugins/ruby-build/bin:$PATH"
eval "$(rbenv init -)"

# install the latest ruby version
rbenv install 2.1.2

# set the latest ruby version globally
rbenv global 2.1.2

# check version of ruby
ruby -v

# set gems to not install any documentation
echo "gem: --no-ri --no-rdoc" > ~/.gemrc

# install bundler
gem install bundler foreman
