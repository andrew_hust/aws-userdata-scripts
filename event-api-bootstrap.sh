#!/bin/bash
set -e -x
export DEBIAN_FRONTEND=noninteractive

# log everything that runs in this script
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1

# mark the start time of the script
echo BEGIN
date '+%Y-%m-%d %H:%M:%S'

# Update, upgrade and install development tools:
apt-get update
apt-get -y upgrade
apt-get -y install ruby python-pip zsh imagemagick libmagickwand-dev awscli zip unzip git-core curl libpq-dev zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties

# add supervisord for process management
easy_install supervisor

git clone https://andrew_hust@bitbucket.org/andrew_hust/aws-userdata-scripts.git
cd aws-userdata-scripts
chmod +x *-install.sh

# install java
./jdk7-install.sh

# Install passenger
# ./passenger-install.sh

# Install rbenv
# sudo -u ubuntu -H -s ./rbenv-install.sh

# mark the end time of the script
date '+%Y-%m-%d %H:%M:%S'
echo END
